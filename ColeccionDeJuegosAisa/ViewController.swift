//
//  ViewController.swift
//  ColeccionDeJuegosAisa
//
//  Created by Mac 05 on 5/12/21.
//  Copyright © 2021 deah. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var juegos : [Juego] = []
    
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return juegos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let juego = juegos[indexPath.row]
        cell.textLabel?.text = juego.titulo
        cell.imageView?.image = UIImage(data : (juego.imagen!))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.dataSource=self
        tableView.delegate=self
        tableView.isEditing=true
        
        
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let siguienteVC = segue.destination as! JuegosViewController
        siguienteVC.juego = sender as? Juego
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            try
                juegos = context.fetch(Juego.fetchRequest())
                tableView.reloadData()
            }catch{
                
            }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let botonEliminar=UITableViewRowAction(style:.normal,title:"Eliminar")
        {(accionesFila,indiceFila) in
            
            let juego = self.juegos[indexPath.row]
            
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            context.delete(juego)
                   
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            do{
               try
                self.juegos = context.fetch(Juego.fetchRequest())
                tableView.reloadData()
               }catch{
             }
               
        
        tableView.reloadData()
        }
        botonEliminar.backgroundColor=UIColor.red
        
        let botonVerMas=UITableViewRowAction(style:.normal,title:"Editar")
        {(accionesFila,indiceFila) in
            let juego = self.juegos[indexPath.row]
            self.performSegue(withIdentifier: "juegoSegue", sender: juego)
            
        }
        botonVerMas.backgroundColor=UIColor.blue
        
        return[botonVerMas, botonEliminar]
        
    }

    func tableView(_ tableView: UITableView,commit editingStyle:UITableViewCell.EditingStyle,forRowAt indexPath: IndexPath)  {
        if editingStyle == .delete{
            let juego = self.juegos[indexPath.row]
            
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            context.delete(juego)
                   
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            do{
               try
                self.juegos = context.fetch(Juego.fetchRequest())
                tableView.reloadData()
               }catch{
             }
            
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to : IndexPath) {
        
        let juegoMovido = juegos[sourceIndexPath.row]
        juegos.remove(at:sourceIndexPath.row)
        juegos.insert(juegoMovido, at: to.row)
        
        
    }
    
 
    
   

}

