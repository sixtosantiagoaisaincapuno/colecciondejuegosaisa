//
//  JuegosViewController.swift
//  ColeccionDeJuegosAisa
//
//  Created by Mac 05 on 5/12/21.
//  Copyright © 2021 deah. All rights reserved.
//

import UIKit

class JuegosViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate {

    
    @IBOutlet var JuegoImageView: UIImageView!
    
    @IBOutlet var titulotextField: UITextField!
    
    
    @IBOutlet var agregarActualizarBoton: UIButton!
    //@IBOutlet var eliminarBoton: UIButton!
       
    
    @IBOutlet weak var seleccionarCategoriaBoton: UIButton!
    
    @IBOutlet weak var pickerTextField: UITextField!
    
    
    var imagePicker=UIImagePickerController()
    var juego:Juego?=nil
    
    let categorias = ["Aventura", "Accion", "Terror", "Shooter"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate=self
        
        
        let pickerView = UIPickerView()
        pickerView.delegate = self               
        pickerTextField.inputView = pickerView
        
        if juego != nil{
            JuegoImageView.image = UIImage(data: (juego!.imagen!) as Data)
            titulotextField.text = juego!.titulo
            pickerTextField.text = juego!.categoria
            agregarActualizarBoton.setTitle("Actualizar", for: .normal)
        }else{
            //eliminarBoton.isHidden=true
        }
      
    }
    
    
    @IBAction func fotosTapped(_ sender: Any){
        imagePicker.sourceType = .photoLibrary
        present(imagePicker,animated: true,completion: nil)
    }
    
    
    
    
    @IBAction func camaraTapped(_ sender: Any) {
        imagePicker.sourceType = .camera
        present(imagePicker,animated: true,completion: nil)
    }
    
    

    @IBAction func agregarTapped(_ sender: Any) {
        
        
        if juego != nil {
            juego!.titulo! = titulotextField.text!
            juego!.imagen = JuegoImageView.image?.jpegData(compressionQuality: 0.50)
            juego!.categoria!=pickerTextField.text!
        }else{
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

            let juego = Juego(context: context)
            juego.titulo=titulotextField.text
            juego.imagen=JuegoImageView.image?.jpegData(compressionQuality: 0.50)
            juego.categoria=pickerTextField.text
          
          
        }
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        navigationController?.popViewController(animated: true)
        
       
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imagenSelecionada=info[.originalImage] as? UIImage
        JuegoImageView.image=imagenSelecionada
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func eliminarTapped(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        context.delete(juego!)
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        navigationController?.popViewController(animated: true)
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return categorias.count
       }
       
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          return categorias[row]
       }
       
       
       func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           pickerTextField.text = categorias[row]
       }
         
    
    
}
